const low = require("lowdb")
const FileSync = require("lowdb/adapters/FileSync")
const adapter = new FileSync("db.json")
const _ = require("lodash")
const axios = require("axios")
const { baseUrl, mediaRoute, apiRoute } = require("./config")
const db = low(adapter)
const bot = require("./bot")
const fs = require("fs")
const cheerio = require("cheerio")
const { filter } = require("fuzzaldrin")
const recursive = require("recursive-readdir")
const path = require("path")

// Print how much heap memory has been allocated and used to
// create an array with all the files contained in a folder
recursive("./bibbia")
  .then(files => {
    const extensions = _.uniq(files.map(file => path.extname(file)))
    console.log(`Extensions: ${extensions}`)
    // const pathPieces = randomPhoto.split("/")
    // const caption = pathPieces[1] + " - " + pathPieces[2].replace(/\W+/g, " ")
    // const used = process.memoryUsage().heapUsed / 1024 / 1024
    // const total = process.memoryUsage().heapTotal / 1024 / 1024
    // const rss = process.memoryUsage().rss / 1024 / 1024
    // console.log(`
    //   used: ${used} mb
    //   total: ${total} mb
    //   rss: ${rss} mb
    // `)
  })
  .catch(err => console.log(err))

// getName = fileName => {
//   const names = _.flattenDeep(
//     fileName
//       .replace(/\d/g, "")
//       .replace(".jpg", "")
//       .split("-")
//       .map(el => el.split("_"))
//   )
//   const filteredNames = names.filter(name => name !== "")
//   const fullName = filteredNames.length > 1 ? filteredNames.join(" ") : ""
//   return fullName
// }

// const fileList = fs.readdirSync("./img")
// const names = _.uniq(fileList.map(getName))
// console.log(filter(fileList, "AshleyLane"))

// const getRandomLink = async category => {
//   const apiUrl = `https://xhamster.com/photos/categories/${category}`
//   let data
//   try {
//     const response = await axios.get(apiUrl)
//     data = response.data
//   } catch (err) {
//     if (error.response) {
//       // The request was made and the server responded with a status code
//       // that falls out of the range of 2xx
//       console.log(error.response.data)
//       console.log(error.response.status)
//       console.log(error.response.headers)
//     } else if (error.request) {
//       // The request was made but no response was received
//       // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
//       // http.ClientRequest in node.js
//       console.log(error.request)
//     } else {
//       // Something happened in setting up the request that triggered an Error
//       console.log("Error", error.message)
//     }
//     console.log(error.config)
//   }
//   const $ = cheerio.load(data)
//   // $(".item.gallery > a > .thumb-container")
//   const elements = $(".thumb>.img.vam>img")
//     .map(function(i, el) {
//       return $(this).attr("src")
//     })
//     .get()
//   const randomLink = _.sample(elements)
//   const bigPictureLink =
//     randomLink.substr(0, randomLink.length - 7) + "1000.jpg"
//   return bigPictureLink
// }

// getRandomLink("bbw")
//   .then(link => console.log(link))
//   .catch(err => console.error(err.message))
// const backupDb = setInterval(() => {
//   fs.copyFile("./db.json", "./db/db_backup_" + Date.now() + ".json", err => {
//     if (err) {
//       console.log("Unable to backup db.json")
//       console.log(err)
//     }
//   })
// }, 2000)

// const element = db
//   .get("sessions")
//   .find(el => el.id.includes("-105872335"))
//   .get("data.usernames")
//   .map("posts")
//   .flattenDeep()
//   .find({ code: "Bbuk1sQH8vY" })
//   .value().likes
// console.log(element)

// const element = db
//   .get("sessions")
//   .find({
//     id: -105872335
//   })
//   .get("data.usernames")
//   .map("posts")
//   .value()
// console.log(element)

// axios
//   .get("https://www.instagram.com/alicemazzolaaaaaa/?__a=1")
//   .then(response => {
//     console.log("Reponse")
//     console.log(response.data)
//   })
//   .catch(error => {
//     if (error.response) {
//       // The request was made and the server responded with a status code
//       // that falls out of the range of 2xx
//       // console.log(error.response.data)
//       console.log(error.response.status)
//       // console.log(error.response.headers)
//       console.log("error response")
//     } else if (error.request) {
//       // The request was made but no response was received
//       // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
//       // http.ClientRequest in node.js
//       // console.log(error.request)
//       console.log("error request")
//     } else {
//       // Something happened in setting up the request that triggered an Error
//       // console.log("Error", error.message)
//       console.log("error something")
//     }
//   })

// const uniqueUsernames = db
//   .get("sessions")
//   .map("data")
//   .map("usernames")
//   .flattenDeep()
//   .map("username")
//   .uniq()
//   .value()

// console.log(uniqueUsernames)
// const link = {
//   date: 11204909,
//   link: "ciao2222",
//   username: "lindseypelas"
// }
// const allIgUsers = db
//   .get("sessions")
//   .map("data")
//   .map("usernames")
//   .flattenDeep()
//   .filter({ username: "xenia" })
//   .map("posts")
//   .flattenDeep()
//   .concat(link)
//   .uniqBy("link")
//   //.filter({ link: "ciao" })
//   // .map(post => ({
//   //   ...post,
//   //   ...link
//   // }))
//   .value()

// const allLinks = db
//   .get("sessions")
//   .find({ id: "44258747:44258747" })
//   .get("data.usernames")
//   .map("posts")
//   .flattenDeep()
//   .value()

// const userToSendPosts = db.get("sessions").value()
// userToSendPosts.map((el, postIndex) => {
//   const tgChatId = el.id
//   if (!el.data.polling) {
//     el.data.usernames.map((username, usernameIndex) => {
//       username.posts.map((post, postIndex) => {
//         if (!post.sent) {
//           bot.telegram.sendMessage(tgChatId, post.link)
//           userToSendPosts[postIndex].data.usernames[usernameIndex].posts[
//             postIndex
//           ].sent = true
//           db.write()
//         }
//       })
//     })
//   }
// })

// const pollLink = async username => {
//   try {
//     const { data } = await axios.get(baseUrl + username + apiRoute)
//     const lastItem = data.user.media.nodes[0]
//     const date = lastItem.date
//     const link = `${baseUrl}p/${lastItem.code}`
//     return { date, link, username }
//   } catch (err) {
//     console.log(err)
//   }
//   return null
// }

//console.log(userToSendPosts)
// const links = pollLink("lindseypelas")
//   .then(result => {
//     console.log(result)
//   })
//   .catch(err => console.log(err))
