const Bottleneck = require("bottleneck")
const limiter = new Bottleneck(2, 2000)
require("./config")
const axios = require("axios")
const Markup = require("telegraf/markup")
const db = require("./dbAdapter").DB
const { baseUrl, mediaRoute, apiRoute } = require("./config")
const _ = require("lodash")
const bot = require("./bot")
const fileDb = require("./db.json")
const fs = require("fs")

const pollLinks = usernames => {
  return Promise.all(usernames.map(pollLink))
}

/**
 * Fetch the latest post from instagram
 * @param {*String} username
 */
const pollLink = async username => {
  try {
    const { data } = await axios.get(baseUrl + username + "/" + apiRoute)
    console.log("Fetching ", baseUrl + username + "/" + apiRoute)
    if (!data.graphql.user.edge_owner_to_timeline_media.edges) {
      return null
    }
    const lastItem =
      data.graphql.user.edge_owner_to_timeline_media.edges[0].node
    const date = lastItem.taken_at_timestamp
    const code = lastItem.shortcode
    const link = `${baseUrl}p/${code}`
    return { date, link, username, code }
  } catch (err) {
    console.log(`${username} not found or no longer valid`)
    console.log(`url:` + baseUrl + username + apiRoute)
  }
  return {
    link: null,
    username
  }
}

/**
 * Merge the fetched posts to the corresponding users
 */
const addPostsToUsers = async () => {
  console.log("Fetching posts")
  const uniqueUsernames = db
    .get("sessions")
    .map("data")
    .map("usernames")
    .flattenDeep()
    .map("username")
    .uniq()
    .value()
  const lastLinks = await pollLinks(uniqueUsernames)
  lastLinks
    .filter(x => {
      if (x && x.link === null) {
        removeUser(x.username)
      } else if (x) {
        return true
      }
      return false
    })
    .map(item => {
      let allIgUsers = db
        .get("sessions")
        .map("data")
        .map("usernames")
        .flattenDeep()
        .filter({ username: item.username })
        .value()
        .map(el => {
          let postAlreadyAdded = false
          el.posts.map(post => {
            if (post.link === item.link) {
              postAlreadyAdded = true
              return { ...post, link: item.link, date: item.date }
            }
            return post
          })
          if (!postAlreadyAdded) {
            el.posts.push({ ...item, likes: 0, dislikes: 0, sent: false })
          }
          // el.posts.unshift({
          //   ...item,
          //   likes: 0,
          //   dislikes: 0
          // })
          // el.posts = _.uniqBy(el.posts, "link")
          return el
        })
      db.write()
    })
}

/**
 * Send all the links with `sent: false` property.
 * Once set, update the values to `sent: true`.
 */
sendPostsToPollingUsers = () => {
  console.log("Sending posts to chats")
  let userToSendPosts = db.get("sessions").value()
  userToSendPosts.map((el, postIndex) => {
    const tgChatId = el.id
    if (el.data.polling !== undefined && el.data.polling) {
      try {
        el.data.usernames.map((username, usernameIndex) => {
          username.posts.map((post, postIndex) => {
            if (!post.sent) {
              bot.telegram
                .sendMessage(
                  tgChatId,
                  post.link,
                  Markup.inlineKeyboard([
                    Markup.callbackButton("❤️", `p${tgChatId}${post.code}`),
                    Markup.callbackButton("💔", `m${tgChatId}${post.code}`)
                  ]).extra()
                )
                .then(() => {
                  el.data.usernames[usernameIndex].posts[postIndex].sent = true
                  db.write()
                })
            }
          })
        })
      } catch (err) {
        return
      }
    }
  })
}

removeUser = toBeDeletedUser => {
  const toWrite = fileDb.sessions.map(chat => {
    if (chat.data.usernames) {
      const usernames = chat.data.usernames.filter(
        username => username.username != toBeDeletedUser
      )
      chat.data.usernames = usernames
    }
    return chat
  })
  fileDb.sessions = toWrite
  fs.writeFile("db.json", JSON.stringify(fileDb, null, 2), "utf8", err => {
    if (err) console.log(err)
  })
}

getUnsentLinks = tgUserChat => {
  return db
    .get("sessions")
    .find({ id: tgUserChat })
    .get("data.usernames")
    .map("posts")
    .flattenDeep()
    .filter({ sent: false })
    .value()
}

setSentLinks = links => {
  return links.map(link => ({
    ...link,
    sent: true
  }))
}

const pollUsers = setInterval(sendPostsToPollingUsers, 70000)
const fetchPosts = setInterval(addPostsToUsers, 50000)
// addPostsToUsers()
module.exports = {
  pollUsers,
  fetchPosts,
  getUnsentLinks,
  setSentLinks
}
