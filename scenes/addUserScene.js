const Scene = require("telegraf/scenes/base")
const Markup = require("telegraf/markup")
const axios = require("axios")
const { baseUrl, mediaRoute, apiRoute } = require("../config")
const bot = require("../bot")
const Stage = require("telegraf/stage")
const { enter, leave } = Stage
const { homeKeyboardLayout } = require("../keyboardlayout")
var Bottleneck = require("bottleneck") // Skip when browser side
var limiter = new Bottleneck(2, 500)

const addUserScene = new Scene("add-user-scene")
addUserScene.enter(ctx =>
  ctx.reply(
    "Insert the username (with the @)",
    Markup.keyboard(["🔙 Back"])
      .resize()
      .extra()
  )
)

addUserScene.hears(/^@([a-z0-9._])+$/, async ctx => {
  const username = ctx.match[0].substring(1)
  if (userIsAlreadyPresent(ctx, username)) {
    return ctx.reply(`You have already added ${username}`)
  }
  let response
  try {
    response = await axios.get(baseUrl + username + apiRoute)
    if (response.data.graphql.user.is_private) {
      return ctx.reply("The user is private. I can't add it")
    }
  } catch (err) {
    if (err.response && err.response.status === 404) {
      return ctx.reply("Username not found")
    }
    console.log(err)
    return ctx.reply("Try again")
  }
  ctx.replyWithMarkdown(`Okay, \`@${username}\` has been added`)
  const igUser = {
    username: response.data.graphql.user.username,
    link: baseUrl + username,
    full_name: response.data.graphql.user.full_name,
    posts: []
  }
  try {
    ctx.session.usernames.push(igUser)
  } catch (error) {
    console.log(error.stack)
  }
})

addUserScene.hears(/(@([a-z0-9._]+))/gm, async ctx => {
  const usernames = ctx.message.text.match(/(@([a-z0-9._]+))/gm)
  if (usernames.length > 10) {
    return ctx.reply("No more than 10 users or instagram will punish me.")
  }
  console.log(usernames)
  ctx.replyWithChatAction("typing")
  const resultText = await Promise.all(
    usernames.map(async username => {
      let response
      const strippedUsername = username.substring(1)
      try {
        if (userIsAlreadyPresent(ctx, strippedUsername)) {
          return `You have already added ${username}`
        }
        response = await limiter.schedule(fetchUser, strippedUsername)
        if (response.data.graphql.user.is_private) {
          return `${username} is private. I cant add it.`
        }
      } catch (err) {
        if (err.response && err.response.status === 404) {
          return `${username} not found.`
        }
        console.log(err)
        return `An error occured for ${username}. Try again.`
      }
      const igUser = {
        username: response.data.graphql.user.username,
        link: baseUrl + strippedUsername,
        full_name: response.data.graphql.user.full_name,
        posts: []
      }
      try {
        ctx.session.usernames.push(igUser)
      } catch (error) {
        console.log(error.stack)
      }
      return `Okay, \`${username}\` has been added`
    })
  )
  ctx.replyWithMarkdown(resultText.join("\n"))
})

const fetchUser = username => {
  return axios.get(baseUrl + username + apiRoute)
}

addUserScene.hears(["/back", "🔙 Back"], ctx => {
  ctx.reply(
    "Getting back",
    Markup.keyboard(homeKeyboardLayout(ctx))
      .oneTime()
      .resize()
      .extra()
  )
  ctx.scene.leave()
})

const userIsAlreadyPresent = (ctx, username) => {
  try {
    const user = ctx.session.usernames.find(el => el.username === username)
    return user ? true : false
  } catch (error) {
    return false
  }
}

module.exports = addUserScene
