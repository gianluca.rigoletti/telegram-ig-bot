const Scene = require("telegraf/scenes/base")
const Markup = require("telegraf/markup")
const { enter, leave } = require("telegraf/stage")
const { homeKeyboardLayout } = require("../keyboardlayout")
const bot = require("../bot")

const settingsScene = new Scene("settings-scene")

settingsScene.enter(ctx => {
  const polling = ctx.session.polling ? "Disable polling" : "Enable polling"
  ctx.reply(
    "What do you want to do?",
    Markup.keyboard([polling, "🔙 Back"])
      .resize()
      .extra()
  )
})

settingsScene.hears("Disable polling", ctx => {
  ctx.session.polling = false
  ctx.reply(
    "Polling disabled",
    Markup.keyboard(["Enable polling", "🔙 Back"])
      .resize()
      .extra()
  )
})

settingsScene.hears("Enable polling", ctx => {
  ctx.session.polling = true
  ctx.reply(
    "Polling enabled. I will check every couple of minutes for updates",
    Markup.keyboard(["Disable polling", "🔙 Back"])
      .resize()
      .extra()
  )
})

settingsScene.hears("🔙 Back", ctx => {
  ctx.reply(
    "Getting back to the menu",
    Markup.keyboard(homeKeyboardLayout(ctx))
      .oneTime()
      .resize()
      .extra()
  )
  ctx.scene.leave()
})

module.exports = settingsScene
