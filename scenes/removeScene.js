const Scene = require("telegraf/scenes/base")
const Markup = require("telegraf/markup")
const bot = require("../bot")
const db = require("../dbAdapter").DB
const { homeKeyboardLayout } = require("../keyboardlayout")

const removeScene = new Scene("remove-scene")

removeScene.enter(ctx => {
  const users = ctx.session.usernames.map(users => `@${users.username}`)
  ctx.reply(
    "Which user do you want to remove?",
    Markup.keyboard([...users, "🔙 Back"])
      .resize()
      .extra()
  )
})

removeScene.hears(/^@([a-z0-9._])+$/, ctx => {
  const username = ctx.match[0].substring(1)
  ctx.session.usernames = ctx.session.usernames.filter(
    el => el.username !== username
  )
  const users = ctx.session.usernames.map(users => `@${users.username}`)
  ctx.reply(
    `${username} removed`,
    Markup.keyboard([...users, "🔙 Back"])
      .resize()
      .extra()
  )
})

removeScene.hears("🔙 Back", ctx => {
  ctx.reply(
    "Ok, cancelled",
    Markup.keyboard(homeKeyboardLayout(ctx))
      .oneTime()
      .resize()
      .extra()
  )
  ctx.scene.leave()
})

module.exports = removeScene
