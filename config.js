const baseUrl = "https://www.instagram.com/"
const mediaRoute = "/media"
const apiRoute = "?__a=1"

module.exports = {
  baseUrl,
  mediaRoute,
  apiRoute
}
