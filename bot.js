const Telegraf = require("telegraf")
require("dotenv").config()
const localSession = require("./dbAdapter")
const bot = new Telegraf(process.env.BOT_TOKEN)
bot.use(localSession).middleware()

module.exports = bot
