const recursive = require("recursive-readdir")
const _ = require("lodash")

// Print how much heap memory has been allocated and used to
// create an array with all the files contained in a folder
recursive("./bibbia")
  .then(files => {
    const randomPhoto = _.sample(files)
    const pathPieces = randomPhoto.split("/")
    const caption = pathPieces[1] + " - " + pathPieces[2].replace(/\W+/g, " ")
    const used = process.memoryUsage().heapUsed / 1024 / 1024
    const total = process.memoryUsage().heapTotal / 1024 / 1024
    const rss = process.memoryUsage().rss / 1024 / 1024
    console.log(`
      used: ${used} mb
      total: ${total} mb
      rss: ${rss} mb
    `)
  })
  .catch(err => console.log(err))
