module.exports = {
  photos: [".jpg", ".jpeg", ".png", ".PNG", ".JPG"],
  videos: [
    ".mp4",
    ".3gp",
    ".avi",
    ".wmv",
    ".MOV",
    ".mov",
    ".m4a",
    ".flv",
    ".gif"
  ],
  others: [".txt", ".ini", ".db", ".pdf", ".zip", ".aac", ".mp3"]
}
