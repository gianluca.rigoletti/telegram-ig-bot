const { photos, videos, others } = require("./bibbiaExtensions")
const path = require("path")

const ignorePhotos = (file, stats) => {
  return photos.includes(path.extname(file))
}

const ignoreVideos = (file, stats) => {
  return videos.includes(path.extname(file))
}

const ignoreOthers = (file, stats) => {
  return others.includes(path.extname(file))
}

module.exports = {
  ignoreOthers,
  ignorePhotos,
  ignoreVideos
}
