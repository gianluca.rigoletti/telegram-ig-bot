const initializePicRatings = ctx => {
  if (ctx.session.ratings === undefined) {
    ctx.session.ratings = {}
  }
}

module.exports = {
  initializePicRatings
}
