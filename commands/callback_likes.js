const bot = require("../bot")
const Markup = require("telegraf/markup")

bot.action(/(p|m)([-]?\d+)(.+)/, ctx => {
  const tgChatId = ctx.update.callback_query.message.chat.id
  const post = ctx["sessionDB"]
    .get("sessions")
    .find({ id: tgChatId })
    .get("data.usernames")
    .map("posts")
    .flattenDeep()
    .find({ code: ctx.match[3] })
    .value()
  if (!post) {
    return ctx.answerCbQuery("An error occured.")
  }
  const currentLikes = post.likes
  const currentDislikes = post.dislikes
  switch (ctx.match[1]) {
    case "p":
      ctx["sessionDB"]
        .get("sessions")
        .find({ id: tgChatId })
        .get("data.usernames")
        .map("posts")
        .flattenDeep()
        .find({ code: ctx.match[3] })
        .assign({ likes: currentLikes + 1 })
        .write()
      break
    case "m":
      const item = ctx["sessionDB"]
        .get("sessions")
        .find({ id: tgChatId })
        .get("data.usernames")
        .map("posts")
        .flattenDeep()
        .find({ code: ctx.match[3] })
        .assign({ dislikes: currentDislikes + 1 })
        .write()
      break
    default:
      break
  }
  return ctx
    .answerCbQuery()
    .then(() =>
      ctx.editMessageText(
        ctx.update.callback_query.message.text,
        Markup.inlineKeyboard([
          Markup.callbackButton(`${post.likes}❤️`, `p${tgChatId}${post.code}`),
          Markup.callbackButton(
            `${post.dislikes}💔`,
            `m${tgChatId}${post.code}`
          )
        ]).extra()
      )
    )
})
