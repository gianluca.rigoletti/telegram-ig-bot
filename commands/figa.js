const bot = require("../bot")
const fs = require("fs")
const fileList = fs.readdirSync("./img")
const { filter } = require("fuzzaldrin")
const _ = require("lodash")
const { initializePicRatings } = require("../utils/initializeDbSchema")

bot.command("figa", ctx => {
  initializePicRatings(ctx)
  const optionalParams = ctx.message.text.replace("/figa", "").trim()
  const modelFileList =
    optionalParams !== "" ? filter(fileList, optionalParams) : fileList
  const randomLink = _.sample(modelFileList)
  if (!randomLink) {
    return ctx.reply("Try again")
  }
  const fullName = getName(randomLink)
  ctx.session.ratings[fullName] = ctx.session.ratings[fullName] | 0
  ctx.session.ratings[fullName]++
  ctx.replyWithChatAction("upload_photo")
  ctx
    .replyWithPhoto(
      { source: fs.createReadStream("./img/" + randomLink) },
      { caption: fullName }
    )
    .catch(err => {
      console.error("There has been an error")
    })
})

getName = fileName => {
  if (!fileName) {
    return "Gnagna"
  }
  const names = _.flattenDeep(
    fileName
      .replace(/\d/g, "")
      .replace(".jpg", "")
      .split("-")
      .map(el => el.split("_"))
  )
  const filteredNames = names.filter(name => name !== "")
  const fullName =
    filteredNames.length > 1 ? filteredNames.join(" ") : filteredNames[0]
  return fullName
}
