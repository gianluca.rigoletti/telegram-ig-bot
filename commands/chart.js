const bot = require("../bot")
const _ = require("lodash")
const outdent = require("outdent")

bot.command("chart", ctx => {
  const localChart = getLocalChart(ctx.session.ratings)
  const chartMessage = getChartMessage(localChart)
  return ctx.replyWithMarkdown(chartMessage)
})

bot.hears("🏆 Chart", ctx => {
  const localChart = getLocalChart(ctx.session.ratings)
  const chartMessage = getChartMessage(localChart)
  return ctx.replyWithMarkdown(chartMessage)
})

const getLocalChart = (userRatings, limit = 5) => {
  return _.sortBy(_.toPairs(userRatings), 1).slice(0, limit)
}

const getChartMessage = sortedPairs => {
  const message = sortedPairs
    .map(pair => {
      return `*${pair[0]}* ${pair[1]}`
    })
    .join("\n")
  return outdent`
    ${message}
  `
}
