/**
 * BTIT: Bigger Than I Thought
 * Fetched few links from the subreddit
 */
const bot = require("../../bot")
const links = require("./gifs")["gifs"]
const _ = require("lodash")

getRandomBtytGif = () => _.sample(links)

bot.hears([/btyt/gi, /surprise/gi], ctx => {
  console.log("Sending btyt")
  ctx.replyWithVideo(getRandomBtytGif()).catch(err => {
    // Retry again two times with another gif
    ctx.replyWithVideo(getRandomBtytGif()).catch(err => {
      ctx
        .replyWithVideo(getRandomBtytGif())
        .catch(err => ctx.reply("Bad luck. Try again?"))
    })
  })
})

bot.command("btyt", ctx => {
  ctx.replyWithVideo(getRandomBtytGif()).catch(err => {
    // Retry again two times with another gif
    ctx.replyWithVideo(getRandomBtytGif()).catch(err => {
      ctx
        .replyWithVideo(getRandomBtytGif())
        .catch(err => ctx.reply("Bad luck. Try again?"))
    })
  })
})
