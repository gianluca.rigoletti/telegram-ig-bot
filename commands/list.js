const bot = require("../bot")
const outdent = require("outdent")

bot.hears(["/list", "📃 List", `/list@${process.env.BOT_NAME}`], ctx => {
  console.log(ctx.session)
  if (!ctx.session.usernames || !ctx.session.usernames.length) {
    return ctx.reply("No usernames added yet")
  }
  const replyMessage = ctx.session.usernames
    .map(users => {
      return outdent`
      *${users.full_name}*
      [${users.username}](${users.link})
    `
    })
    .join("\n\n")
  ctx.replyWithMarkdown(replyMessage)
})
