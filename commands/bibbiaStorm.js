const bot = require("../bot")
const recursive = require("recursive-readdir")
const _ = require("lodash")
const { photos, videos, others } = require("../utils/bibbiaExtensions")
const {
  ignorePhotos,
  ignoreVideos,
  ignoreOthers
} = require("../utils/ignoreFunctions")
const path = require("path")

bot.command("bibbiastorm", ctx => {
  recursive("./bibbia")
    .then(files => {
      const randomPhotos = _.sampleSize(files, 5)
      ctx.replyWithChatAction("upload_photo")
      const replyMessage = randomPhotos.map(randomPhoto => {
        const pathPieces = randomPhoto.split("/")
        const caption =
          pathPieces[1] + " - " + pathPieces[2].replace(/\W+/g, " ")
        let type
        if (videos.includes(path.basename(randomPhoto))) {
          type = "video"
        } else {
          type = "photo"
        }
        return {
          media: { source: randomPhoto },
          caption,
          type
        }
      })
      ctx.replyWithMediaGroup(replyMessage)
    })
    .catch(err => console.log(err))
})
