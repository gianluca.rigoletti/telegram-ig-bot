const bot = require("../../bot")
const links = require("./links")["links"]
const _ = require("lodash")

getRandomLinks = () => _.sampleSize(links, 5)

bot.hears([/olgastorm$/gi, /\/olgastorm$/gi], ctx => {
  const links = getRandomLinks()
  const mediaGroup = links.map(el => {
    const type = el.includes("gfycat") ? "video" : "photo"
    return { media: el, type }
  })
  ctx.replyWithChatAction("upload_photo")
  ctx
    .replyWithMediaGroup(mediaGroup)
    .catch(err => console.log("there has been an error sending olgastorm"))
})
