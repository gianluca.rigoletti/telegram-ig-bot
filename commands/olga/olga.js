const bot = require("../../bot")
const links = require("./links")["links"]
const _ = require("lodash")

getRandomLink = () => _.sample(links)

bot.hears([/olga$/gi, /\/olga$/gi], ctx => {
  const link = getRandomLink()
  if (link.includes("gfycat")) {
    ctx.replyWithChatAction("upload_video")
    return ctx.replyWithVideo(link)
  }
  ctx.replyWithChatAction("upload_photo")
  return ctx.replyWithPhoto(link)
})
