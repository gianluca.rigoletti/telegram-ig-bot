const bot = require("../bot")
const Markup = require("telegraf/markup")

bot.hears(["/poll", "Poll", `/poll@${process.env.BOT_NAME}`], ctx => {
  if (ctx.session.polling) {
    return ctx.reply(
      "You need first to disable automatic poll in the settings."
    )
  }
  const tgChatId = ctx.message.chat.id
  const linkElements = getUnsentLinks(tgChatId)
  linkElements.map(el => {
    ctx.reply(
      el.link,
      Markup.inlineKeyboard([
        Markup.callbackButton(`${el.likes}❤️`, `+${tgChatId}${el.code}`),
        Markup.callbackButton(`${el.dislikes}💔`, `-${tgChatId}${el.code}`)
      ]).extra()
    )
  })
})
