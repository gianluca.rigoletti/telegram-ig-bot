const bot = require("../bot")
const recursive = require("recursive-readdir")
const _ = require("lodash")
const { photos, videos, others } = require("../utils/bibbiaExtensions")
const {
  ignorePhotos,
  ignoreVideos,
  ignoreOthers
} = require("../utils/ignoreFunctions")
const path = require("path")

bot.command("bibbia", ctx => {
  console.log(ctx.message.text)
  const sendVideo = /video/gi.test(ctx.message.text)
  sendVideo
    ? recursive("./bibbia", [ignorePhotos, ignoreOthers]).then(files => {
        const randomVideo = _.sample(files)
        ctx.replyWithChatAction("upload_video")
        ctx.replyWithVideo({ source: randomVideo })
      })
    : recursive("./bibbia", [ignoreOthers])
        .then(files => {
          const randomPhoto = _.sample(files)
          const pathPieces = randomPhoto.split("/")
          const caption =
            pathPieces[1] + " - " + pathPieces[2].replace(/\W+/g, " ")
          ctx.replyWithChatAction("upload_photo")
          if (videos.includes(path.basename(randomPhoto))) {
            ctx.replyWithVideo({ source: randomPhoto }, { caption })
          } else {
            ctx.replyWithPhoto({ source: randomPhoto }, { caption })
          }
        })
        .catch(err => console.log(err))
})
