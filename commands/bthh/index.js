/**
 * BTHH: Bigger Than Her Head
 * Fetched few links from the subreddit
 */
const bot = require("../../bot")
const links = require("./gifs")["gifs"]
const _ = require("lodash")

getRandomBthhGif = () => _.sample(links)

bot.hears(/bthh/gi, ctx => {
  ctx.replyWithVideo(getRandomBthhGif()).catch(err => {
    // Retry again two times with another gif
    ctx.replyWithVideo(getRandomBthhGif()).catch(err => {
      ctx.replyWithVideo(getRandomBthhGif()).catch(err => {
        console.error(err)
        ctx.reply("Bad luck. Try again?")
      })
    })
  })
})

bot.command("btyt", ctx => {
  ctx.replyWithVideo(getRandomBthhGif()).catch(err => {
    // Retry again two times with another gif
    ctx.replyWithVideo(getRandomBthhGif()).catch(err => {
      ctx
        .replyWithVideo(getRandomBthhGif())
        .catch(err => ctx.reply("Bad luck. Try again?"))
    })
  })
})
