const bot = require("../bot")

bot.hears(
  ["/settings", "⚙️ Settings", `/settings@${process.env.BOT_NAME}`],
  ctx => {
    ctx.scene.enter("settings-scene")
  }
)
