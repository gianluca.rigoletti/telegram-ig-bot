const bot = require("../bot")
const { filter } = require("fuzzaldrin")
const _ = require("lodash")
const cheerio = require("cheerio")
const axios = require("axios")

const categories = [
  "amateur",
  "anal",
  "gifs",
  "asian",
  "asses",
  "babes",
  "bbw",
  "bdsm",
  "beach",
  "big-boobs",
  "bisexuals",
  "ebony",
  "blondes",
  "blowjobs",
  "brunettes",
  "cameltoes",
  "cartoons",
  "celebs",
  "close-ups",
  "creampie",
  "cumshots",
  "facials",
  "femdom",
  "fingering",
  "flashing",
  "funny",
  "group",
  "hairy",
  "handjobs",
  "hardcore",
  "hidden",
  "interracial",
  "latin",
  "lesbians",
  "man",
  "masturbation",
  "matures",
  "milfs",
  "nipples",
  "nonporn",
  "old-young",
  "pissing",
  "pornstars",
  "public",
  "redheads",
  "toys",
  // "shemale",
  "showers",
  "stockings",
  "teens",
  "tits",
  "upskirts",
  "vintage",
  "voyeur",
  "webcams"
]

bot.hears(/\/xhamster$/, async ctx => {
  const randomCategory = _.sample(categories)
  console.log(randomCategory)
  const link = await getRandomLink(randomCategory)
  if (link) {
    ctx.reply(`${randomCategory}\n${link}`)
  } else {
    ctx.reply("error")
  }
})

bot.hears(/\/xhamster(\s?)([\w\s]+)$/, async ctx => {
  const match = ctx.message.text.match(/\/xhamster(\s?)([\w\s]+)$/)
  const category = match[2]
  console.log(category)
  const fuzzyMatch = filter(categories, category)[0]
  const link = await getRandomLink(fuzzyMatch)
  if (link) {
    ctx.reply(`${fuzzyMatch}\n${link}`)
  }
})

const getRandomLink = async category => {
  const apiUrl = `https://xhamster.com/photos/categories/${category}`
  let data
  try {
    const response = await axios.get(apiUrl)
    data = response.data
  } catch (err) {
    console.log(err)
    return null
  }
  const $ = cheerio.load(data)
  // $(".item.gallery > a > .thumb-container")
  const elements = $(".thumb>.img.vam>img")
    .map(function(i, el) {
      return $(this).attr("src")
    })
    .get()
  if (!elements.length) {
    return null
  }
  const randomLink = _.sample(elements)
  const bigPictureLink =
    randomLink.substr(0, randomLink.length - 7) + "1000.jpg"
  return bigPictureLink
}
