const bot = require("../bot")
const Markup = require("telegraf/markup")
const { homeKeyboardLayout } = require("../keyboardlayout")

bot.hears(["/start", `/start@${process.env.BOT_NAME}`], ctx => {
  ctx.reply(
    "This bot sends you updates of people from instagram. Start by adding some instagram usernames",
    Markup.keyboard(homeKeyboardLayout(ctx))
      .oneTime()
      .resize()
      .extra()
  )
  ctx.session.polling = ctx.session.polling | true
  ctx.session.usernames =
    ctx.session.usernames && ctx.session.usernames.length
      ? ctx.session.usernames
      : []
})
