const bot = require("../bot")
const _ = require("lodash")
const fs = require("fs")
const fileList = fs.readdirSync("./img")
const { filter } = require("fuzzaldrin")
const { initializePicRatings } = require("../utils/initializeDbSchema")

bot.command("figastorm", ctx => {
  initializePicRatings(ctx)
  const optionalParams = ctx.message.text.replace("/figastorm", "").trim()
  const modelFileList =
    optionalParams !== "" ? filter(fileList, optionalParams) : fileList

  const firstLink = modelFileList[0]
  const fullName = getName(firstLink)
  ctx.session.ratings[fullName] = ctx.session.ratings[fullName] | 0
  ctx.session.ratings[fullName]++

  const randomStorm = _.sampleSize(modelFileList, 5)
  const mediaGroup = randomStorm.map(el => {
    const fullName = getName(el)
    return {
      media: {
        source: "./img/" + el
      },
      caption: fullName,
      type: "photo"
    }
  })
  ctx.replyWithChatAction("upload_photo")
  ctx
    .replyWithMediaGroup(mediaGroup)
    .catch(err => console.log("there has been an error"))
})

getName = fileName => {
  if (!fileName) {
    return "Gnagna"
  }
  const names = _.flattenDeep(
    fileName
      .replace(/\d/g, "")
      .replace(".jpg", "")
      .split("-")
      .map(el => el.split("_"))
  )
  const filteredNames = names.filter(name => name !== "")
  const fullName =
    filteredNames.length > 1 ? filteredNames.join(" ") : filteredNames[0]
  return fullName
}
