const db = require("../dbAdapter")

const homeKeyboardLayout = ctx => {
  const isPolling = ctx.session.polling
  console.log(ctx.session.polling)
  return isPolling
    ? [["➕ Add", "📃 List"], ["🚫 Remove", "⚙️ Settings"], ["🏆 Chart"]]
    : [
        ["➕ Add", "📃 List"],
        ["🚫 Remove", "⚙️ Settings"],
        ["🏆 Chart"],
        ["Poll"]
      ]
}

module.exports = { homeKeyboardLayout }
