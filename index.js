const Telegraf = require("telegraf")
const Markup = require("telegraf/markup")
const Stage = require("telegraf/stage")
const { enter, leave } = Stage
const outdent = require("outdent")
require("dotenv").config()
const { addUserScene, removeScene, settingsScene } = require("./scenes")
const stage = new Stage([addUserScene, removeScene, settingsScene])
const bot = require("./bot")
const { pollUsers, getUnsentLinks, setSentLinks } = require("./pollUsers")
const { backupDb } = require("./db/backup")
const { homeKeyboardLayout } = require("./keyboardlayout")
bot.use(stage.middleware())
require("./commands")

// bot.on("callback_query", ctx => {
//   console.log(ctx.update.callback_query.data)
// })

bot.catch(err => {
  console.log("Error occured:")
  console.log(err)
})

bot.startPolling()

process.on("unhandledRejection", function(reason, p) {
  console.log(
    "Possibly Unhandled Rejection at: Promise ",
    p,
    " reason: ",
    reason
  )
})

process.on("uncaughtException", function(err) {
  // handle the error safely
  console.log(err.stack)
  process.exit(1)
})
