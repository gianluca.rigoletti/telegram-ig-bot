const LocalSession = require("telegraf-session-local")
const localSession = new LocalSession({
  database: "db.json",
  storage: LocalSession.storagefileSync,
  getSessionKey: ctx => {
    let chatInstance = ctx.chat.id
    if (ctx.chat) {
      chatInstance = ctx.chat.id
    } else if (ctx.updateType === "callback_query") {
      chatInstance = ctx.update.callback_query.message.chat.id
    } else {
      // if (ctx.updateType === 'inline_query') {
      chatInstance = ctx.chat.id
    }
    return chatInstance
  }
})

module.exports = localSession
