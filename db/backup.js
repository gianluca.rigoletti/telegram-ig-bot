const fs = require("fs")

const createBackup = () => {
  return new Promise((res, rej) => {
    fs.copyFile(
      "./db.json",
      "./db/backup/db_backup_" + Date.now() + ".json",
      err => {
        console.log("Db backup")
        if (err) {
          rej(err)
          console.log("Unable to backup db.json")
          console.log(err)
        }
        res()
      }
    )
  })
}

const backupDb = setInterval(async () => {
  try {
    await createBackup()
  } catch (error) {
    console.log(error)
  }
}, 300000)

module.exports = {
  backupDb,
  createBackup
}
