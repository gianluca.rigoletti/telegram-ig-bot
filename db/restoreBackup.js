const fs = require("fs")
const { createBackup } = require("./backup")

fs.readdir("./db/backup", async (err, data) => {
  if (err) {
    console.log(err)
    process.exit(1)
  }
  const fileList = data.filter(el => el !== ".gitkeep")
  const dates = fileList.map(el => el.split("_")[2].split(".")[0]).reverse()
  let backupToRestore = parseInt(process.argv[2]) || 1
  const dateToPick = dates[backupToRestore - 1]
  const fileToPick = `db_backup_${dateToPick}.json`
  try {
    await createBackup()
  } catch (err) {
    console.log(err)
  }
  console.log("Restoring", fileToPick, "to db.json")
  fs.copyFile(`./db/backup/${fileToPick}`, "./db.json", err => {
    if (err) {
      console.log(err)
    }
  })
})
